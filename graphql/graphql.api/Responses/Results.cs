﻿using System.Collections.Generic;
namespace trivia.api.Responses
{
    public class Results<T>
    {
        public List<T> Data { get; set; }
        public string CorrelationId { get; set; }
        
        public Results()
        {

        }

        public Results(List<T> data, string correlationId)
        {
            this.Data = data;
            this.CorrelationId = correlationId;
        }
    }
}
