﻿using trivia.api.Models;
using trivia.api.Requests;
using trivia.api.Responses;
using System.Threading.Tasks;

namespace trivia.api.Services
{
    public interface ITopicService
    {
        Task<Results<Topic>> GetRandomList(GetRandomListRequest request);
        Task<Results<Topic>> GetAll(BaseRequest request);
        Task<Result<Topic>> GetById(GetOrDeleteBaseRequest request);

        Task<Result<Topic>> Add(AddOrUpdateBaseRequest<Topic> request);
        Task<Result<Topic>> Update(AddOrUpdateBaseRequest<Topic> request);
        Task Delete(GetOrDeleteBaseRequest request);
    }
}
