﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using trivia.api.Models;
using trivia.api.Requests;
using trivia.api.Responses;

namespace trivia.api.Services
{
    public interface IQuestionService
    {
        Task<Results<Question>> GetRandomList(GetRandomListRequest request);
        Task<Results<Question>> GetAllByTopicId(GetQuestionsByTopicIdRequest request);
        Task<ILookup<long, Question>> GetLookupForQuestionsByTopicId(IEnumerable<long> topicIds);

        Task<Result<Question>> GetById(GetOrDeleteBaseRequest request);

        Task<Result<Question>> Add(AddOrUpdateBaseRequest<Question> request);
        Task<Result<Question>> Update(AddOrUpdateBaseRequest<Question> request);
        Task Delete(GetOrDeleteBaseRequest request);
    }
}
