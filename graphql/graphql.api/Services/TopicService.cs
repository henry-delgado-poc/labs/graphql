﻿using trivia.api.Models;
using trivia.api.Requests;
using trivia.api.Responses;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace trivia.api.Services
{
    public class TopicService : ITopicService
    {
        private readonly ApplicationDbContext _context;

        public TopicService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Result<Topic>> Add(AddOrUpdateBaseRequest<Topic> request)
        {
            if (string.IsNullOrEmpty(request?.Data?.Name))
                throw new ArgumentNullException($"{nameof(request)}");

            var found = await _context.Topic.FirstOrDefaultAsync(x => x.Name.ToUpper() == request.Data.Name.ToUpper()) != null;
            if (found)
                throw new ArgumentException($"{nameof(request.Data.Name)}");

            await _context.Topic.AddAsync(request.Data);
            await _context.SaveChangesAsync();

            return new Result<Topic>(request.Data, request.CorrelationId);
        }

        public async Task Delete(GetOrDeleteBaseRequest request)
        {

            if (request?.Id < 1)
                throw new ArgumentNullException($"{nameof(request)}");
            
            var data = await _context.Topic.FirstOrDefaultAsync(x => x.Id == request.Id);
            if (data == null)
                throw new ArgumentException($"{nameof(request.Id)}");

            _context.Topic.Remove(data);
            await _context.SaveChangesAsync();
        }

        public async Task<Results<Topic>> GetAll(BaseRequest request)
        {
            var data = await _context.Topic.OrderBy(x=> Guid.NewGuid()).ToListAsync();
            return new Results<Topic>(data, request.CorrelationId);
        }

        public async Task<Result<Topic>> GetById(GetOrDeleteBaseRequest request)
        {
            if (request?.Id < 1)
                throw new ArgumentNullException($"{nameof(request)}");

            var data = await _context.Topic.FirstOrDefaultAsync(x => x.Id == request.Id);
            if (data == null)
                throw new ArgumentException($"{nameof(request.Id)}");

            return new Result<Topic>(data, request.CorrelationId);
        }

        public async Task<Results<Topic>> GetRandomList(GetRandomListRequest request)
        {
            if (request.Count < 1)
                request.Count = 1;

            var data = await _context.Topic.OrderBy(x => Guid.NewGuid()).Take(request.Count).ToListAsync();
            return new Results<Topic>(data, request.CorrelationId);
        }

        public async Task<Result<Topic>> Update(AddOrUpdateBaseRequest<Topic> request)
        {
            if (string.IsNullOrEmpty(request?.Data?.Name))
                throw new ArgumentNullException($"{nameof(request)}");

            if(request?.Data.Id <1)
                throw new ArgumentNullException($"{nameof(request)}");

            var found = await _context.Topic.FirstOrDefaultAsync(x => x.Name.ToUpper() == request.Data.Name.ToUpper()) != null;
            if (!found)
                throw new ArgumentException($"{nameof(request.Data.Name)}");

            _context.Entry(request.Data).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return new Result<Topic>(request.Data, request.CorrelationId);
        }
    }
}
