﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using trivia.api.Models;
using trivia.api.Requests;
using trivia.api.Responses;

namespace trivia.api.Services
{
    public class QuestionService: IQuestionService
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<QuestionService> _logger;

        public QuestionService(ApplicationDbContext context, ILogger<QuestionService> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<Result<Question>> Add(AddOrUpdateBaseRequest<Question> request)
        {
            if (string.IsNullOrEmpty(request?.Data?.Text))
                throw new ArgumentNullException($"{nameof(request)}");

            var found = await _context.Question.FirstOrDefaultAsync(x => x.Text.ToUpper() == request.Data.Text.ToUpper()) != null;
            if (found)
                throw new ArgumentException($"{nameof(request.Data.Text)}");

            await _context.Question.AddAsync(request.Data);
            await _context.SaveChangesAsync();

            return new Result<Question>(request.Data, request.CorrelationId);
        }

        public async Task Delete(GetOrDeleteBaseRequest request)
        {
            if (request?.Id < 1)
                throw new ArgumentNullException($"{nameof(request)}");

            var data = await _context.Question.FirstOrDefaultAsync(x => x.Id == request.Id);
            if (data == null)
                throw new ArgumentException($"{nameof(request.Id)}");

            _context.Question.Remove(data);
            await _context.SaveChangesAsync();
        }

        public async Task<Results<Question>> GetAllByTopicId(GetQuestionsByTopicIdRequest request)
        {
            if (request.TopicId < -1)
                throw new ArgumentNullException($"{nameof(request)}");

            var data = await _context.Question.Where(q => q.TopicId == request.TopicId).OrderBy(x => Guid.NewGuid()).ToListAsync();
            return new Results<Question>(data, request.CorrelationId);
        }

        public async Task<Result<Question>> GetById(GetOrDeleteBaseRequest request)
        {
            if (request?.Id < 1)
                throw new ArgumentNullException($"{nameof(request)}");

            var data = await _context.Question.FirstOrDefaultAsync(x => x.Id == request.Id);
            if (data == null)
                throw new ArgumentException($"{nameof(request.Id)}");

            return new Result<Question>(data, request.CorrelationId);
        }

        public async Task<ILookup<long, Question>> GetLookupForQuestionsByTopicId(IEnumerable<long> topicIds)
        {
            _logger.LogInformation($"Getting questions related to given topics [{string.Join(",", topicIds)}]");
            if (topicIds == null || !topicIds.Any())
                throw new ArgumentNullException($"{nameof(Topic)}");

            var data = await _context.Question.Where(x=> topicIds.Contains(x.TopicId)).OrderBy(x => Guid.NewGuid()).ToListAsync();
            return data.ToLookup(t => t.TopicId);
        }

        public async Task<Results<Question>> GetRandomList(GetRandomListRequest request)
        {
            if (request.Count < 1)
                request.Count = 1;

            var data = await _context.Question.OrderBy(x => Guid.NewGuid()).Take(request.Count).ToListAsync();
            return new Results<Question>(data, request.CorrelationId);
        }

        public async Task<Result<Question>> Update(AddOrUpdateBaseRequest<Question> request)
        {
            if (string.IsNullOrEmpty(request?.Data?.Text))
                throw new ArgumentNullException($"{nameof(request)}");

            if (request?.Data.Id < 1)
                throw new ArgumentNullException($"{nameof(request)}");

            var found = await _context.Question.FirstOrDefaultAsync(x => x.Text.ToUpper() == request.Data.Text.ToUpper()) != null;
            if (!found)
                throw new ArgumentException($"{nameof(request.Data.Text)}");

            _context.Entry(request.Data).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return new Result<Question>(request.Data, request.CorrelationId);
        }
    }
}
