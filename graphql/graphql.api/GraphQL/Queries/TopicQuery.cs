﻿using trivia.api.GraphQL.Types;
using trivia.api.Services;
using GraphQL.Types;
using System;
using GraphQL;

namespace trivia.api.GraphQL.Queries
{
    public class TopicQuery: ObjectGraphType
    {
        public TopicQuery(ITopicService service)
        {
            FieldAsync<ListGraphType<TopicType>>(
                "topics",
                resolve: async context =>
                   {
                       var result = await service.GetAll(new Requests.BaseRequest() { CorrelationId = Guid.NewGuid().ToString() });
                       return result.Data;
                   }
                    
                );

            FieldAsync<TopicType>(
                "topic",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id" }),
                resolve: async context =>
                {
                    var id = context.GetArgument<long>("id");
                    var request = new Requests.GetOrDeleteBaseRequest
                    {
                        CorrelationId = "ABC",
                        Id = id
                    };
                    var result = await service.GetById(request);
                    return result.Data;
                }
                );

            FieldAsync<ListGraphType<TopicType>>(
                "randomTopics",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "count" }),
                resolve: async context =>
                {
                    var count = context.GetArgument<int>("count");
                    var request = new Requests.GetRandomListRequest
                    {
                        CorrelationId = "ABC",
                        Count = count
                    };
                    var result = await service.GetRandomList(request);
                    return result.Data;
                }
                );
        }
    }
}
