﻿using trivia.api.Models;
using GraphQL.Types;
using trivia.api.Services;
using GraphQL.DataLoader;
using trivia.api.Requests;
using trivia.api.Responses;

namespace trivia.api.GraphQL.Types
{
    public class TopicType:ObjectGraphType<Topic>
    {
        public TopicType(IQuestionService questionService, IDataLoaderContextAccessor dataLoaderContextAccessor)
        {
            Field(t => t.Id);
            Field(t => t.Name).Description("The name of the topic");
            Field<StringGraphType>("CorrelationId");
            Field<ListGraphType<QuestionType>>(
                "questions",
                resolve: context =>
                {
                    var dataLoader = 
                        dataLoaderContextAccessor.Context.GetOrAddCollectionBatchLoader<long, Question>(
                            "GetQuestionsByTopicId",
                            questionService.GetLookupForQuestionsByTopicId);
                    
                    return dataLoader.LoadAsync(context.Source.Id);
                });
        }
    }
}
