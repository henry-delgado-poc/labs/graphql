﻿using trivia.api.Models;
using GraphQL.Types;

namespace trivia.api.GraphQL.Types
{
    public class QuestionType: ObjectGraphType<Question>
    {
        public QuestionType()
        {
            Field(t => t.Id);
            Field(t => t.Text).Description("The question to ask");
            Field(t => t.TopicId).Description("The topic Id related to this question");
            //todo: add topic name

        }
    }
}
