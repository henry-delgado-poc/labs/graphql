﻿using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;
using System;
using trivia.api.GraphQL.Queries;

namespace trivia.api.GraphQL
{
    public class TriviaSchema: Schema
    {
        public TriviaSchema(IServiceProvider provider) : base(provider)
        {
            Query = provider.GetRequiredService<TopicQuery>();
        }
    }
}
