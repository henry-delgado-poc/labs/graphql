﻿namespace trivia.api.Requests
{
    public class GetOrDeleteBaseRequest : BaseRequest
    {
        public long Id { get; set; }
    }
}
