﻿using System.Collections.Generic;

namespace trivia.api.Requests
{
    public class GetQuestionsByTopicLookupRequest: BaseRequest
    {
        public IEnumerable<long> TopicIds { get; set; }
    }
}
