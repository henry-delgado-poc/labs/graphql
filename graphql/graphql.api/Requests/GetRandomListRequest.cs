﻿namespace trivia.api.Requests
{
    public class GetRandomListRequest: BaseRequest
    {
        public int Count { get; set; }
    }
}
