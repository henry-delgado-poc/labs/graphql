﻿namespace trivia.api.Requests
{
    public class GetQuestionsByTopicIdRequest: BaseRequest
    {
        public long TopicId { get; set; }
    }
}
