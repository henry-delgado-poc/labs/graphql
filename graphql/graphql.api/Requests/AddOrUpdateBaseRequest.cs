﻿namespace trivia.api.Requests
{
    public class AddOrUpdateBaseRequest<T> : BaseRequest
    {
        public T Data { get; set; }

        public AddOrUpdateBaseRequest()
        {

        }

        public AddOrUpdateBaseRequest(T data)
        {
            Data = data;
        }
    }
}
