﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using trivia.api.Models;
using trivia.api.Requests;
using trivia.api.Responses;
using trivia.api.Services;

namespace trivia.api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TopicsController : ControllerBase
    {
        private readonly ITopicService _service;

        public TopicsController(ITopicService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<ActionResult<Results<Topic>>> Get()
        {
            return await _service.GetAll(new Requests.BaseRequest { CorrelationId = "ABC" });
        }

        [HttpPost]
        public async Task<ActionResult<Result<Topic>>> Post(AddOrUpdateBaseRequest<Topic> request)
        {
            return await _service.Add(request);
        }
    }
}
