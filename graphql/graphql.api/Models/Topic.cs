﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace trivia.api.Models
{
    public class Topic
    {
        [Key]
        public long Id { get; set; }
        
        [Required]
        [MaxLength(300)]
        public string Name { get; set; }

        public virtual ICollection<Question> Questions { get; set; }

        public Topic(long id, string name)
        {
            Id = id;
            Name = name;
        }

        public Topic()
        {

        }
    }
}
