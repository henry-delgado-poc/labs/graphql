using trivia.api.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using trivia.api.GraphQL;
using GraphQL;
using GraphQL.MicrosoftDI;
using System;
using GraphQL.Server;
using GraphQL.NewtonsoftJson;
using trivia.api.GraphQL.Queries;

namespace trivia.api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson(options => options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);
            var dbConnectionString = Configuration.GetConnectionString("Default");
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(dbConnectionString).UseLazyLoadingProxies());

            services.AddSingleton<IDocumentWriter, DocumentWriter>();
            services.AddSingleton<IDocumentExecuter, DocumentExecuter>();
            services.AddTransient<ITopicService, TopicService>();
            services.AddTransient<IQuestionService, QuestionService>();
            services.AddTransient<TopicQuery>();
            //Ensure you do not create a service for the type. Use just the following instead
            services.AddScoped<TriviaSchema>(services => new TriviaSchema(new SelfActivatingServiceProvider(services)));

            services.AddGraphQL(options =>
            {
                options.EnableMetrics = true;
                options.UnhandledExceptionDelegate = ctx => { Console.WriteLine(ctx.OriginalException); };
            })
            .AddErrorInfoProvider(opt => opt.ExposeExceptionStackTrace = true)
            .AddSystemTextJson(JsonSerializerSettings => { }, JsonSerializerSettings => { })
            .AddDataLoader(); //allows more efficient queries. Otherwise you call data multiple times. Sort of cache on GraphQL
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            app.UseGraphQL<TriviaSchema>();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseGraphQLPlayground();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
