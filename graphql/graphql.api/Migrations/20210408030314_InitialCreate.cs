﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace trivia.api.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "trivia");

            migrationBuilder.CreateTable(
                name: "Topic",
                schema: "trivia",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Topic", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Question",
                schema: "trivia",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TopicId = table.Column<long>(type: "bigint", nullable: false),
                    Text = table.Column<string>(type: "nvarchar(800)", maxLength: 800, nullable: true),
                    AnswerExplanation = table.Column<string>(type: "nvarchar(800)", maxLength: 800, nullable: true),
                    AnswerUrlReference = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Question", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Question_Topic_TopicId",
                        column: x => x.TopicId,
                        principalSchema: "trivia",
                        principalTable: "Topic",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Answer",
                schema: "trivia",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    QuestionId = table.Column<long>(type: "bigint", nullable: false),
                    Text = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    IsCorrect = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Answer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Answer_Question_QuestionId",
                        column: x => x.QuestionId,
                        principalSchema: "trivia",
                        principalTable: "Question",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "QuestionStat",
                schema: "trivia",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    QuestionId = table.Column<long>(type: "bigint", nullable: false),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Player = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    AnsweredCorrectly = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuestionStat", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuestionStat_Question_QuestionId",
                        column: x => x.QuestionId,
                        principalSchema: "trivia",
                        principalTable: "Question",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Answer_QuestionId",
                schema: "trivia",
                table: "Answer",
                column: "QuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_Answer_Text",
                schema: "trivia",
                table: "Answer",
                column: "Text");

            migrationBuilder.CreateIndex(
                name: "IX_Question_Text",
                schema: "trivia",
                table: "Question",
                column: "Text");

            migrationBuilder.CreateIndex(
                name: "IX_Question_TopicId",
                schema: "trivia",
                table: "Question",
                column: "TopicId");

            migrationBuilder.CreateIndex(
                name: "IX_QuestionStat_QuestionId",
                schema: "trivia",
                table: "QuestionStat",
                column: "QuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_Topic_Name",
                schema: "trivia",
                table: "Topic",
                column: "Name");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Answer",
                schema: "trivia");

            migrationBuilder.DropTable(
                name: "QuestionStat",
                schema: "trivia");

            migrationBuilder.DropTable(
                name: "Question",
                schema: "trivia");

            migrationBuilder.DropTable(
                name: "Topic",
                schema: "trivia");
        }
    }
}
